package dev.tacon.datastructures.graph;

public interface Vertex<V> {

	V getValue();
}
