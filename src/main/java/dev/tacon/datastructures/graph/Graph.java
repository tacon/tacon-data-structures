package dev.tacon.datastructures.graph;

import java.util.ArrayList;
import java.util.Collection;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;

public interface Graph<V extends Vertex<T>, T, E> {

	@NonNull
	V addVertex(T v);

	boolean removeVertex(@NonNull V v, boolean onlyAlone);

	@Nullable
	E getEdge(@NonNull V from, @NonNull V to);

	@Nullable
	E setEdge(@NonNull V from, @NonNull V to, @Nullable E edge);

	@Nullable
	E removeEdge(@NonNull V from, @NonNull V to);

	@NonNull
	Collection<V> getVertices();

	@NonNull
	Collection<Edge<V, E>> outgoingEdgesOf(@NonNull SimpleVertex<T> vertex);

	@NonNull
	Collection<Edge<V, E>> incomingEdgesOf(@NonNull SimpleVertex<T> vertex);

	@NonNull
	default Collection<Edge<V, E>> edgesOf(final @NonNull SimpleVertex<T> vertex) {
		final Collection<Edge<V, E>> outgoingEdgesOf = this.outgoingEdgesOf(vertex);
		final Collection<Edge<V, E>> incomingEdgesOf = this.incomingEdgesOf(vertex);
		final Collection<Edge<V, E>> edges = new ArrayList<>(outgoingEdgesOf.size() + incomingEdgesOf.size());
		edges.addAll(outgoingEdgesOf);
		edges.addAll(incomingEdgesOf);
		return edges;
	}

}