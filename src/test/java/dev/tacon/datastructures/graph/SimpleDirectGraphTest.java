package dev.tacon.datastructures.graph;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;

import org.junit.jupiter.api.Test;

public class SimpleDirectGraphTest {

	@Test
	public void addNode() {
		final SimpleDirectedGraph<String, Integer> graph = new SimpleDirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		final Collection<SimpleVertex<String>> nodes = graph.getVertices();
		assertEquals(3, nodes.size());
		assertTrue(nodes.contains(rome));
		assertTrue(nodes.contains(venice));
		assertTrue(nodes.contains(sdp));

		assertEquals("Rome", rome.getValue());
	}

	@Test
	public void setEdge() {
		final SimpleDirectedGraph<String, Integer> graph = new SimpleDirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		assertNull(graph.setEdge(rome, venice, Integer.valueOf(526)));
		assertNull(graph.setEdge(venice, rome, Integer.valueOf(528)));
		assertNull(graph.setEdge(venice, sdp, Integer.valueOf(51)));

		assertEquals(Integer.valueOf(526), graph.getEdge(rome, venice));
		assertEquals(Integer.valueOf(528), graph.getEdge(venice, rome));
		assertEquals(Integer.valueOf(51), graph.getEdge(venice, sdp));
		assertNull(graph.getEdge(sdp, venice));

		assertEquals(Integer.valueOf(526), graph.setEdge(rome, venice, Integer.valueOf(500)));

		final SimpleDirectedGraph<String, Integer> graph2 = new SimpleDirectedGraph<>();
		final SimpleVertex<String> rome2 = graph2.addVertex("Rome");

		assertThrows(IllegalStateException.class, () -> graph.setEdge(rome2, venice, Integer.valueOf(526)));
		assertThrows(IllegalStateException.class, () -> graph.getEdge(rome2, venice));
		assertThrows(IllegalStateException.class, () -> graph.getEdge(venice, rome2));
	}

	@Test
	public void removeEdge() {
		final SimpleDirectedGraph<String, Integer> graph = new SimpleDirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		graph.setEdge(rome, venice, Integer.valueOf(526));
		graph.setEdge(venice, rome, Integer.valueOf(528));
		graph.setEdge(venice, sdp, Integer.valueOf(51));

		assertNull(graph.removeEdge(rome, sdp));
		assertEquals(Integer.valueOf(526), graph.removeEdge(rome, venice));
		assertNull(graph.getEdge(rome, venice));
	}

	@Test
	public void removeNode() {
		final SimpleDirectedGraph<String, Integer> graph = new SimpleDirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");
		final SimpleVertex<String> alone = graph.addVertex("Alone");

		graph.setEdge(rome, venice, Integer.valueOf(526));
		graph.setEdge(venice, rome, Integer.valueOf(528));
		graph.setEdge(venice, sdp, Integer.valueOf(51));

		assertThrows(IllegalStateException.class, () -> graph.removeVertex(venice, true));

		assertFalse(graph.removeVertex(venice, false));
		assertThrows(IllegalStateException.class, () -> graph.removeVertex(venice, false));

		assertTrue(graph.removeVertex(alone, false));
		assertThrows(IllegalStateException.class, () -> graph.removeVertex(alone, false));

		final SimpleVertex<String> alone1 = graph.addVertex("Alone");
		graph.removeVertex(alone1, true);
		assertThrows(IllegalStateException.class, () -> graph.removeVertex(alone1, false));

	}

	@Test
	public void getNeighbours() {
		final SimpleDirectedGraph<String, Integer> graph = new SimpleDirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		graph.setEdge(rome, venice, Integer.valueOf(526));
		graph.setEdge(venice, rome, Integer.valueOf(528));
		graph.setEdge(venice, sdp, Integer.valueOf(51));

		final Collection<Edge<SimpleVertex<String>, Integer>> outgoingEdgesOfVenice = graph.outgoingEdgesOf(venice);
		assertEquals(2, outgoingEdgesOfVenice.size());
		assertTrue(outgoingEdgesOfVenice.contains(new Edge<>(venice, rome, Integer.valueOf(528), true)));
		assertTrue(outgoingEdgesOfVenice.contains(new Edge<>(venice, sdp, Integer.valueOf(51), true)));

		final Collection<Edge<SimpleVertex<String>, Integer>> incomingEdgesOfVenice = graph.incomingEdgesOf(venice);
		assertEquals(1, incomingEdgesOfVenice.size());
		assertTrue(incomingEdgesOfVenice.contains(new Edge<>(rome, venice, Integer.valueOf(526), true)));

		final Collection<Edge<SimpleVertex<String>, Integer>> edgesOfVenice = graph.edgesOf(venice);
		assertEquals(3, edgesOfVenice.size());
		assertTrue(edgesOfVenice.contains(new Edge<>(venice, rome, Integer.valueOf(528), true)));
		assertTrue(edgesOfVenice.contains(new Edge<>(venice, sdp, Integer.valueOf(51), true)));
		assertTrue(edgesOfVenice.contains(new Edge<>(rome, venice, Integer.valueOf(526), true)));

		final SimpleDirectedGraph<String, Integer> graph2 = new SimpleDirectedGraph<>();
		final SimpleVertex<String> rome2 = graph2.addVertex("Rome");
		assertThrows(IllegalStateException.class, () -> graph.outgoingEdgesOf(rome2));
		assertThrows(IllegalStateException.class, () -> graph.incomingEdgesOf(rome2));
		assertThrows(IllegalStateException.class, () -> graph.edgesOf(rome2));

	}
}
