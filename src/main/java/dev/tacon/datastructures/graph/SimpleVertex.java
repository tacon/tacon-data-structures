package dev.tacon.datastructures.graph;

public final class SimpleVertex<T> implements Vertex<T> {

	private final T value;

	SimpleVertex(final T value) {
		this.value = value;
	}

	@Override
	public T getValue() {
		return this.value;
	}
}
