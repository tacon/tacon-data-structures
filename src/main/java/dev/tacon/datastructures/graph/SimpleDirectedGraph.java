package dev.tacon.datastructures.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;

public class SimpleDirectedGraph<T, E> implements Graph<SimpleVertex<T>, T, E> {

	private final Set<SimpleVertex<T>> vertices = new HashSet<>();

	private final Map<Vertexes<T>, E> edges = new HashMap<>();

	private final boolean directed;

	public SimpleDirectedGraph() {
		this(true);
	}

	protected SimpleDirectedGraph(final boolean directed) {
		this.directed = directed;
	}

	@Override
	public @NonNull SimpleVertex<T> addVertex(final T v) {
		final SimpleVertex<T> vertex = new SimpleVertex<>(v);
		this.vertices.add(vertex);
		return vertex;
	}

	@Override
	public boolean removeVertex(final @NonNull SimpleVertex<T> v, final boolean onlyAlone) {
		if (!this.vertices.contains(v)) {
			throw new IllegalStateException("Vertex doesn't exist");
		}

		final List<Vertexes<T>> edgesKey = this.getEdgesKey(v).map(Entry::getKey).collect(Collectors.toList());
		if (!edgesKey.isEmpty() && onlyAlone) {
			throw new IllegalStateException("Vertex has edges");
		}
		edgesKey.forEach(this.edges::remove);
		this.vertices.remove(v);
		return edgesKey.isEmpty();
	}

	@Override
	public E setEdge(final @NonNull SimpleVertex<T> from, final @NonNull SimpleVertex<T> to, final @Nullable E edge) {
		final Vertexes<T> pair = this.getVerticesPair(from, to);
		return this.edges.put(pair, edge);
	}

	@Override
	public E removeEdge(final @NonNull SimpleVertex<T> from, final @NonNull SimpleVertex<T> to) {
		final Vertexes<T> pair = this.getVerticesPair(from, to);
		return this.edges.remove(pair);
	}

	@Override
	public @NonNull Collection<SimpleVertex<T>> getVertices() {
		return Collections.unmodifiableCollection(this.vertices);
	}

	@Override
	public @NonNull Collection<Edge<SimpleVertex<T>, E>> outgoingEdgesOf(final @NonNull SimpleVertex<T> vertex) {
		if (!this.vertices.contains(vertex)) {
			throw new IllegalStateException("Vertex doesn't exist");
		}
		return this.getEdgesKey(vertex)
				.filter(e -> e.getKey().getVertex1() == vertex)
				.map(e -> {
					final E value = e.getValue();
					final Vertex<T> vertex1 = e.getKey().getVertex1();
					final Vertex<T> vertex2 = e.getKey().getVertex2();
					@SuppressWarnings({ "unchecked", "rawtypes" })
					final Edge<SimpleVertex<T>, E> edge = new Edge(vertex1, vertex2, value, this.directed);
					return edge;
				})
				.collect(Collectors.toList());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public @NonNull Collection<Edge<SimpleVertex<T>, E>> incomingEdgesOf(final @NonNull SimpleVertex<T> vertex) {
		if (!this.vertices.contains(vertex)) {
			throw new IllegalStateException("Vertex doesn't exist");
		}
		return this.getEdgesKey(vertex)
				.filter(e -> e.getKey().getVertex2() == vertex)
				.map(e -> new Edge(e.getKey().getVertex1(), e.getKey().getVertex2(), e.getValue(), this.directed))
				.collect(Collectors.toList());
	}

	@Override
	public E getEdge(final @NonNull SimpleVertex<T> from, final @NonNull SimpleVertex<T> to) {
		final Vertexes<T> pair = this.getVerticesPair(from, to);
		return this.edges.get(pair);
	}

	private Vertexes<T> getVerticesPair(final SimpleVertex<T> from, final SimpleVertex<T> to) {
		if (!this.vertices.contains(from)) {
			throw new IllegalStateException("Vertex \"from\" doesn't exist");
		}
		if (!this.vertices.contains(to)) {
			throw new IllegalStateException("Vertex \"to\" doesn't exist");
		}
		return new Vertexes<>(from, to);
	}

	private Stream<Entry<Vertexes<T>, E>> getEdgesKey(final SimpleVertex<T> vertex) {
		return this.edges.entrySet().stream().filter(e -> {
			final Vertexes<T> k = e.getKey();
			return k.getVertex1() == vertex || k.getVertex2() == vertex;
		});
	}

	public boolean isDirected() {
		return this.directed;
	}
}
