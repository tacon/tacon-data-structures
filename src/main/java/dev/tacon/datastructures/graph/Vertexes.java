package dev.tacon.datastructures.graph;

import java.util.Objects;

public class Vertexes<T> {

	private final Vertex<T> vertex1;
	private final Vertex<T> vertex2;

	public Vertexes(final Vertex<T> vertex1, final Vertex<T> vertex2) {
		super();
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
	}

	public Vertex<T> getVertex1() {
		return this.vertex1;
	}

	public Vertex<T> getVertex2() {
		return this.vertex2;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.vertex1, this.vertex2);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Vertexes)) {
			return false;
		}
		@SuppressWarnings("rawtypes")
		final Vertexes other = (Vertexes) obj;
		return Objects.equals(this.vertex1, other.vertex1) && Objects.equals(this.vertex2, other.vertex2);
	}

}
