package dev.tacon.datastructures.graph;

import java.util.Objects;

public class Edge<V extends Vertex<?>, E> {

	private final V vertex1;
	private final V vertex2;
	private final E value;
	private final boolean directed;

	public Edge(final V vertex1, final V vertex2, final E edgeValue, final boolean directed) {
		super();
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.value = edgeValue;
		this.directed = directed;

	}

	public V getVertex1() {
		return this.vertex1;
	}

	public V getVertex2() {
		return this.vertex2;
	}

	public E getValue() {
		return this.value;
	}

	public boolean isDirected() {
		return this.directed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.directed ? 1231 : 1237);
		result = prime * result + (this.value == null ? 0 : this.value.hashCode());
		if (this.directed) {
			result = prime * result + (this.vertex1 == null ? 0 : this.vertex1.hashCode());
			result = prime * result + (this.vertex2 == null ? 0 : this.vertex2.hashCode());
		} else {
			final int v1 = this.vertex1 == null ? 0 : this.vertex1.hashCode();
			final int v2 = this.vertex2 == null ? 0 : this.vertex2.hashCode();
			if (v1 > v2) {
				result = prime * result + v1;
				result = prime * result + v2;
			} else {
				result = prime * result + v2;
				result = prime * result + v1;
			}
		}
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		@SuppressWarnings("rawtypes")
		final Edge other = (Edge) obj;
		if (this.directed != other.directed) {
			return false;
		}
		if (!Objects.equals(this.value, other.value)) {
			return false;
		}
		if (this.directed) {
			return Objects.equals(this.vertex1, other.vertex1) && Objects.equals(this.vertex2, other.vertex2);
		}
		return Objects.equals(this.vertex1, other.vertex1) && Objects.equals(this.vertex2, other.vertex2) || Objects.equals(this.vertex1, other.vertex2) && Objects.equals(this.vertex2, other.vertex1);
	}

}
