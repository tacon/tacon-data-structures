package dev.tacon.datastructures.graph;

import java.util.Collection;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;

public class SimpleUndirectedGraph<V, E> extends SimpleDirectedGraph<V, E> {

	public SimpleUndirectedGraph() {
		super(false);
	}

	@Override
	public E setEdge(final @NonNull SimpleVertex<V> from, final @NonNull SimpleVertex<V> to, final @Nullable E edge) {
		final E e1 = super.setEdge(from, to, edge);
		final E e2 = super.setEdge(to, from, edge);
		assert e1 == e2;
		return e1;
	}

	@Override
	public E removeEdge(final @NonNull SimpleVertex<V> from, final @NonNull SimpleVertex<V> to) {
		final E e1 = super.removeEdge(from, to);
		final E e2 = super.removeEdge(to, from);
		assert e1 == e2;
		return e1;
	}

	@Override
	@NonNull
	public Collection<Edge<SimpleVertex<V>, E>> edgesOf(@NonNull final SimpleVertex<V> vertex) {
		return this.incomingEdgesOf(vertex);
	}
}
